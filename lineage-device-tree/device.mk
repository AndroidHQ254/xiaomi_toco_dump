#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Enable updating of APEXes
$(call inherit-product, $(SRC_TARGET_DIR)/product/updatable_apex.mk)

# Include GSI keys
$(call inherit-product, $(SRC_TARGET_DIR)/product/gsi_keys.mk)

# fastbootd
PRODUCT_PACKAGES += \
    android.hardware.fastboot@1.1-impl-mock \
    fastbootd

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-impl.recovery \
    android.hardware.health@2.1-service

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Partitions
PRODUCT_BUILD_SUPER_PARTITION := false
PRODUCT_USE_DYNAMIC_PARTITIONS := true

# Product characteristics
PRODUCT_CHARACTERISTICS := default

# Rootdir
PRODUCT_PACKAGES += \
    init.qcom.sh \
    init.qcom.early_boot.sh \
    init.qti.chg_policy.sh \
    init.class_main.sh \
    us_cal.sh \
    init.mdm.sh \
    playback.sh \
    init.qcom.coex.sh \
    setup_topmic2headphone.sh \
    capture_headset.sh \
    teardown_loopback.sh \
    init.qcom.post_boot.sh \
    init.qti.qcv.sh \
    init.qcom.efs.sync.sh \
    mishow.sh \
    init.qti.dcvs.sh \
    setup_mainmic2headphone.sh \
    install-recovery.sh \
    init.qcom.class_core.sh \
    qca6234-service.sh \
    init.qcom.sdio.sh \
    init.qcom.sensors.sh \
    playback_headset.sh \
    init.mi.usb.sh \
    setup_headsetmic2headphone.sh \
    init.qcom.usb.sh \
    capture.sh \
    init.crda.sh \

PRODUCT_PACKAGES += \
    fstab.emmc \
    init.qti.ufs.rc \
    init.qcom.usb.rc \
    init.target.rc \
    init.qcom.rc \
    init.qcom.factory.rc \

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/rootdir/etc/fstab.emmc:$(TARGET_COPY_OUT_RAMDISK)/fstab.emmc

# Shipping API level
PRODUCT_SHIPPING_API_LEVEL := 29

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/xiaomi/toco/toco-vendor.mk)
