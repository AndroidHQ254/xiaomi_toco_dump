## qssi-user 12
12 SKQ1.210908.001 V13.0.4.0.SFNMIXM release-keys
- Manufacturer: xiaomi
- Platform: sm6150
- Codename: toco
- Brand: Xiaomi
- Flavor: qssi-user
- Release Version: 12
12
- Kernel Version: 4.14.190
- Id: SKQ1.210908.001
- Incremental: V13.0.4.0.SFNMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Xiaomi/toco_global/toco:12/RKQ1.210614.002/V13.0.4.0.SFNMIXM:user/release-keys
- OTA version: 
- Branch: qssi-user-12
12-SKQ1.210908.001-V13.0.4.0.SFNMIXM-release-keys
- Repo: xiaomi_toco_dump
